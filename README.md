# Mammoth

A beautiful Mastodon client for Android.

## Initially Planned Features

You can see what's planned by looking at Mammoth's [milestones](https://gitlab.com/MammothApp/mammoth/milestones).

## FAQ\*

**Why Mammoth?**

I'm from Kentucky.
Mastodons look like elephants.
Mammoths look like elephants.
The largest cave in the United States (Mammoth Cave) is in Kentucky.

**Why Flutter instead of Java/Kotlin/React Native?**

I like Flutter's performance.
I don't like Java.
I like Dart more than Kotlin.
I already know React really well and would like to grow :)

**What's the inspiration for creating this?**

The fine folks who make Tweetbot and Twitterific are possibly making a Mastodon client, but only for iOS.
I use Android for my phone and there isn't a Mastodon client that is as nice as one that Tapbots/IconFactory would make, so I thought I'd give it a go.
I also wanted to create a real Flutter app.

<sup>\*No one has ever asked me these questions.</sup>

> <div>Icon made by <a href="http://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
