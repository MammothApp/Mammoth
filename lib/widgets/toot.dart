import 'package:flutter/material.dart';

class Toot extends StatefulWidget {
  _TootState createState() => _TootState();
}

class _TootState extends State<Toot> {
  bool isOpen = false;

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints(minHeight: 128.0),
      child: Container(
        margin: EdgeInsets.only(bottom: 8.0),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border(
            top: BorderSide.none,
            left: BorderSide.none,
            right: BorderSide.none,
            bottom: BorderSide(color: Color(0xFFEEEEEE), width: 2.0),
          ),
        ),
      ),
    );
  }
}
