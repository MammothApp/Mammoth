import 'package:flutter/material.dart';
import 'package:mammoth/models/account.dart';

class NameBar extends StatelessWidget {
  final Account account;

  NameBar({this.account});

  @override
  build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        left: 16.0,
        right: 16.0,
        top: 16.0,
        bottom: 16.0,
      ),
      color: Theme.of(context).accentColor,
      child: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(right: 16.0),
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.white,
                          width: 3.0,
                        ),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                            color: Colors.black12,
                            offset: Offset(0.0, 2.0),
                            blurRadius: 1.0,
                          )
                        ],
                        borderRadius: BorderRadius.circular(60.0),
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(
                            account.avatar,
                          ),
                        ),
                      ),
                      width: 96.0,
                      height: 96.0,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(0.0, 4.0, 0.0, 4.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          '@' + account.username,
                          style:
                              TextStyle(color: Colors.white70, fontSize: 18.0),
                        ),
                        Text(
                          account.displayName,
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 20.0),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Material(
              elevation: 2.0,
              borderRadius: BorderRadius.circular(3.0),
              child: IconButton(
                color: Theme.of(context).accentColor,
                onPressed: () {},
                icon: Icon(Icons.person_add),
                padding: EdgeInsets.all(0.0),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
