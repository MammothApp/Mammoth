import 'package:mammoth/data/authentication/app.dart';

class PersistApp {
  final AppResponse app;
  final String instanceName;

  PersistApp(this.instanceName, this.app);
}

class AuthState {
  Map<String, AppResponse> apps;

  AuthState({
    this.apps = const {},
  });
}

AuthState authReducer(AuthState state, action) {
  if (action is PersistApp) {
    return AuthState(
        apps: Map.from(state.apps)
          ..putIfAbsent(action.instanceName, () => action.app));
  }
}
