import 'package:redux/redux.dart';
import './auth.dart' as auth;

final store = new Store(auth.authReducer, initialState: auth.AuthState());
