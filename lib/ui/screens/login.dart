import 'package:flutter/material.dart';
import 'package:flutter_statusbar_manager/flutter_statusbar_manager.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:mammoth/services/mastodon.dart';
import 'package:mammoth/data/authentication/app.dart';

class LoginScreen extends StatelessWidget {
  Future _setColors() async {
    try {
      await FlutterStatusbarManager.setStyle(StatusBarStyle.LIGHT_CONTENT);
      await FlutterStatusbarManager.setNavigationBarColor(Color(0xFF212834));
      await FlutterStatusbarManager.setNavigationBarStyle(
          NavigationBarStyle.LIGHT);
    } catch (e) {
      print(e);
    }
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    _setColors();

    String _instanceName = '';
    http.BaseClient httpClient = http.Client();

    _handleLoginSubmit() async {
      _formKey.currentState.save();
      Mastodon mastodon = new Mastodon(
        client: httpClient,
        instanceName: _instanceName,
      );
      AppResponse mastodonApp = await mastodon.createNewApp();
      String clientId = mastodonApp.clientId;
      final String url =
          "https://$_instanceName/oauth/authorize?client_id=$clientId&response_type=code&scope=read+write+follow+push&redirect_uri=urn:ietf:wg:oauth:2.0:oob";
      final webview = FlutterWebviewPlugin();
      await webview.launch(url, clearCache: true, clearCookies: true);
      webview.onUrlChanged.listen((String url) {
        Uri uri = Uri.parse(url);
        if (uri.queryParameters['code'] is String) {
          // We have the access token!
          // var code = uri.queryParameters['code'];
          webview.close();
        }
      });
    }

    return Scaffold(
      body: Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                'Mammoth',
                style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.w900),
              ),
              TextFormField(
                onSaved: (String value) {
                  _instanceName = value;
                },
                validator: (String validator) {
                  _instanceName = validator;
                },
                keyboardType: TextInputType.url,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.symmetric(vertical: 8.0),
                  labelText: 'Mastodon Site Name',
                  hintText: 'eg. mastodon.social',
                ),
                autocorrect: false,
                autofocus: true,
              ),
              RaisedButton(
                onPressed: _handleLoginSubmit,
                color: Colors.blue,
                child: Text('Login'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
