import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import 'package:mammoth/store/store.dart';

// Views
import 'package:mammoth/views/settings.dart';
import 'package:mammoth/views/profile.dart';
import 'package:mammoth/ui/screens/login.dart';

// Widgets
import 'package:mammoth/ui/components/main_bottom_nav.dart';

// Models
import 'package:mammoth/models/account.dart';

Account account = Account(
  id: '1',
  avatar: 'https://source.unsplash.com/random/128x128',
  username: 'steveude',
  displayName: 'Steve The Guy',
  header: 'https://source.unsplash.com/random/960x540',
);

class MammothApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreProvider(
      store: store,
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          fontFamily: "Nunito",
          brightness: Brightness.dark,
          primaryColor: Colors.white,
          accentColor: Colors.blue,
          accentColorBrightness: Brightness.dark,
          canvasColor: Color(0xFF293240),
        ),
        home: LoginScreen(),
        routes: <String, WidgetBuilder>{
          '/settings': (BuildContext context) => SettingsView(),
          '/test-profile': (BuildContext context) => ProfileView(
                account: account,
              )
        },
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.blueAccent,
        foregroundColor: Colors.white,
        onPressed: () {
          Navigator.pushNamed(context, "/settings");
        },
        child: Icon(Icons.create),
      ),
      resizeToAvoidBottomPadding: false,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: MainBottomNav(),
    );
  }
}
