import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:mammoth/data/authentication/app.dart';
import 'package:mammoth/store/auth.dart';

/// Container for interracting with a Mastodon instance's API.
class Mastodon {
  final latestVersion = 'v1';
  final String instanceName;
  final http.Client client;

  Mastodon({this.client, this.instanceName});

  get apiEndpoint => "https://$instanceName/api/$latestVersion";

  /// Register an application on a Mastodon instance.
  ///
  /// We do this because in order for Mammoth to work on every instance, we need an OAuth app created on every instance.
  /// We create one using this method.
  /// This created app should be persisted so we don't keep creating DB records.
  ///
  /// It can possibly also live in the Mammoth service-layer as an ETS table.
  /// For further reading, look here:
  ///
  /// https://git.io/fAOAR
  Future createNewApp() async {
    var response = await client.post(
      "$apiEndpoint/apps",
      body: AppRequest(
              clientName: 'Mammoth for Mastodon',
              redirectUris: 'urn:ietf:wg:oauth:2.0:oob http://localhost:1337',
              scopes: 'read write follow push',
              website: 'https://mammothapp.co')
          .toJson(),
    );

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      var app = AppResponse(
          clientId: data['client_id'],
          clientSecret: data['client_secret'],
          id: data['id']);
      PersistApp(this.instanceName, app);
      return app;
    }
    // Exception if any bad status code occurs
    else {
      throw Exception("""Failed to create application.

Status Code: ${response.statusCode}
Body: ${response.body}
""");
    }
  }
}
