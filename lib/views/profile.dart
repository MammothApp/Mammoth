import 'package:flutter/material.dart';
import 'package:mammoth/widgets/toot.dart';
import 'package:mammoth/widgets/profile/name_bar.dart';
import 'package:mammoth/models/account.dart';

class ProfileView extends StatelessWidget {
  final Account account;

  ProfileView({this.account});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        scrollDirection: Axis.vertical,
        slivers: <Widget>[
          SliverAppBar(
            iconTheme: IconThemeData(color: Colors.white),
            expandedHeight: 160.0,
            pinned: true,
            floating: false,
            brightness: Brightness.dark,
            backgroundColor: Colors.blue,
            flexibleSpace: FlexibleSpaceBar(
              title: Text(
                account.displayName,
                style: TextStyle(color: Colors.white),
              ),
              background: Image.network(
                account.header,
                height: 160.0,
                fit: BoxFit.cover,
                color: Colors.blue.withOpacity(0.2),
                colorBlendMode: BlendMode.dstATop,
              ),
            ),
          ),
          // User Avatar/Name/Follow Button
          SliverToBoxAdapter(
            child: NameBar(account: account),
          ),
          SliverToBoxAdapter(child: Toot()),
          SliverToBoxAdapter(child: Toot()),
          SliverToBoxAdapter(child: Toot()),
          SliverToBoxAdapter(child: Toot()),
          SliverToBoxAdapter(child: Toot()),
          SliverToBoxAdapter(child: Toot()),
          SliverToBoxAdapter(child: Toot()),
          SliverToBoxAdapter(child: Toot()),
          SliverToBoxAdapter(child: Toot()),
        ],
      ),
    );
  }
}
