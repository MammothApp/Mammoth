import 'package:flutter/material.dart';

class SettingsView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Settings'),
      ),
      body: ListView(children: [
        ListTile(
          title: Text('Howdy!'),
          subtitle: Text('Woooah'),
          trailing: Switch(
            value: false,
            onChanged: (bool) {},
          ),
        ),
      ]),
    );
  }
}
