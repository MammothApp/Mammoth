class Account {
  final String id;
  final String username;
  final String displayName;
  final String avatar;
  final String url;
  final String header;
  final String headerStatic;
  final String moved;

  Account({
    this.id,
    this.username,
    this.displayName,
    this.avatar,
    this.url,
    this.header,
    this.headerStatic,
    this.moved,
  });
}
