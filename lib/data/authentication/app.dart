class AppRequest {
  final String clientName;
  final String redirectUris;
  final String scopes;
  final String website;

  AppRequest({this.clientName, this.redirectUris, this.scopes, this.website});

  Map<String, dynamic> toJson() => {
        'client_name': clientName,
        'redirect_uris': redirectUris,
        'scopes': scopes,
        'website': website
      };
}

class AppResponse {
  final String clientId;
  final String clientSecret;
  final String id;

  AppResponse({this.clientId, this.clientSecret, this.id});
}
