import './account.dart';

Account loginAccount({Account account}) {}
void logoutAccount({Account account}) {}
Account blockAccount({Account account}) {}
Account muteAccount({Account account}) {}
Account unmuteAccount({Account account}) {}
Account reportAccount({Account account}) {}
Account lockAccount({Account account}) {}
Account makeAccountBot({Account account}) {}
Account changeAccountDisplayName() {}
Account changeAccountAvatar() {}
Account changeAccountHeader() {}
Account changeAccountMetadata() {}
