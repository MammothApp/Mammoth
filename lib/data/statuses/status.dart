import 'dart:async';

enum Visibility {
  Direct,
  Private,
  Unlisted,
  Public,
}

class DraftStatus {
  String status;
  String inReplyToId;
  List<String> mediaIds;
  bool sensitive;
  String spoilerText;
  Visibility visibility;
  String language;

  DraftStatus({
    this.status,
    this.inReplyToId,
    this.mediaIds,
    this.sensitive,
    this.spoilerText,
    this.visibility,
    this.language,
  });
}

class Status {}

Future<Status> createStatus(DraftStatus status) async {}
