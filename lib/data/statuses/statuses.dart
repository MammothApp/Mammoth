import 'package:mammoth/data/accounts/account.dart';
import './status.dart' as Status;

createStatus(Status.DraftStatus status) async {
  Status.Status createdStatus = await Status.createStatus(status);
  return createdStatus;
}

void deleteStatus({Status.Status status}) {}
Status.Status favoriteStatus({Status.Status status}) {}
Status.Status boostStatus({Status.Status status}) {}
Status.Status replyStatus({Status.Status status}) {}
Status.Status showSensitiveContent() {}
Status.Status hideSensitiveContent() {}
Status.Status createDirectMessage({Account to, Status.Status message}) {}
