import 'package:test/test.dart';
import 'package:mammoth/data/authentication/app.dart';
import 'package:mammoth/store/auth.dart';

main() {
  test('reducer', () {
    var initialState = {
      'mastodon.social': AppResponse(
        clientId: '',
        clientSecret: '',
        id: '',
      )
    };

    var reducedState = authReducer(
      AuthState(apps: initialState),
      PersistApp(
        'mastodon.social',
        AppResponse(
          clientId: 'asdf',
          clientSecret: 'asdf',
          id: 'asdf',
        ),
      ),
    );

    expect(reducedState.apps, initialState);
  });
}
