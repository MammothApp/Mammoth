import 'dart:convert';
import 'package:test/test.dart';
import 'package:http/http.dart' as http;
import 'package:mockito/mockito.dart';
import 'package:mammoth/data/authentication/app.dart';
import 'package:mammoth/services/mastodon.dart';
import 'package:uuid/uuid.dart';

class MockClient extends Mock implements http.Client {}

void main() {
  group('apps', () {
    test('createNewApp works with a 200', () async {
      final client = MockClient();

      var uuid = Uuid();

      var clientId = uuid.v4();
      var clientSecret = uuid.v4();
      var id = uuid.v4();

      when(client.post('https://mastodon.social/api/v1/apps',
              headers: anyNamed("headers"), body: anyNamed("body")))
          .thenAnswer((Invocation invocation) async => http.Response(
              jsonEncode({
                'client_id': clientId,
                'client_secret': clientSecret,
                'id': id,
              }),
              200));

      final Matcher isAppResponse = TypeMatcher<AppResponse>();

      var mastodon = Mastodon(client: client, instanceName: 'mastodon.social');
      var app = await mastodon.createNewApp();

      // Make sure we're getting back an app
      expect(app, isAppResponse);
      // Make sure the values are correct
      expect(app.clientId, clientId);
      expect(app.clientSecret, clientSecret);
      expect(app.id, id);
    });

    test(
      'createNewApp throws an exception when we get anything other than a 200',
      () async {
        final client = MockClient();

        when(client.post('https://mastodon.social/api/v1/apps',
                headers: anyNamed("headers"), body: anyNamed("body")))
            .thenAnswer((_) async => http.Response('RUH ROH', 500));

        var mastodon =
            Mastodon(client: client, instanceName: 'mastodon.social');
        var app = mastodon.createNewApp();

        expect(app, throwsException);
      },
    );
  });
}
