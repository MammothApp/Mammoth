# Contributing to Mammoth

### Up and Running

Make sure you have [Flutter] installed for your operating system.
Do note that I have only worked on Mammoth using macOS.
I also have only used [Visual Studio Code] for developing Mammoth, so I'm unaware of how to work on Mammoth using Android Studio.

[flutter]: https://flutter.io/
[visual studio code]: https://code.visualstudio.com/

### Development

#### Editor Integration

If you use Visual Studio Code, you can run the app on an Android device/emulator with a launch task I've included in the repo.
I believe Android Studio (when set up following the Flutter guides on the website) can run the project without issues as well.

#### Everything Else

Everything else is able to run like a normal Flutter app using normal Flutter command line commands.

### Filing Issues

File as many issues you see fit.
If the issue you are filing is a feature request, please do not be offended if I mark it as "will not do."

There is only one person maintaining Mammoth, so any features that are too complex to maintain will fall on that person and can be very time consuming considering all of the other features that need to be maintained.
Lots of discussion and planning has to be had in order to justify the development overhead when new features are proposed that have not already been planned by the Mammoth maintainers.

### Submitting PRs

Please do not create a PR without an issue being filed and it being marked as "will do" or otherwise asked for.
This is only asked because it saves all of us time when vetting through the issue tracker and PRs.

If you are submitting a planned PR, our CI process should catch most of everything necessary to pass our human review.
So, please make sure CI is green and passing across the board before pinging anyone to look at the PR—for all of our sakes.

Once a human is looking at it, please do not be offended by how thorough a review can be.
We vet ourselves just as thorough, and we only ask to change things in order for all code to be consistent across the code base—it is not about making anyone feel inadequate about their programming abilities.
A lot of times most of the review will be nitpicks that can be relatively easy to fix.

With that said, it is important to know that **_you should not be afraid to ask questions on how to fix something that was requested in a review._**
Generally, if we asked for a fix, it is because we have an idea of how to implement that fix, but leave the implementation up to the requestor for time saving purposes.
If you need **_any_** help **_what-so-ever_**, please do not hesitate to ask how we envisioned a fix being implemented.
